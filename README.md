#Frozen Fist

##VortexOps

We are currently working on a project codenamed **"Frozen Fist."**  WE are designing software for semi-autonomous armored assault vehicles. The vehicles are designed to operate either **remotely** with a *human pilot* or in **"robot mode"** with guidance from an on-board Artificial Intelligence (AI) system.

Contact info: Joe Nadeau  
Email [joseph.nadeau@smail.rasmussen.edu](https://joseph.nadeau@smail.rasmussen.edu)

![https://p-trc.obsidianportal.com/wikis/order-of-the-iron-fist](http://cdn.obsidianportal.com/assets/212768/gauntlet-wings-logo.jpg)

###Pushed to BitBucket Repository -
Modified README on BitBucket - 02/25/18